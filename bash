export CLICOLOR=1

if [ -f ~/.bash_path ] ; then
	source ~/.bash_path
fi

name_kill() {
	if [[ -z "$2" ]]; then
		processList=`ps -x | grep $1 | grep -v grep `
	else
		processList=`ps -x | grep $1 | grep -v grep | grep -v $2`
	fi

	echo "$processList" | sed 's/^/	/g' | sed 's/^.*:..\... /	/g'

	read -r -p "Remove? (Y/n)" response
	response=`tr [A-Z] [a-z] <<< "$response"`
	if [[ "yes y " =~ (^|[[:space:]])"$response"($|[[:space:]]) ]] ; then
		echo "$processList" | sed 's/ ?.*//g' | xargs kill
		echo "Done."
	else
		echo "Aborted."
	fi
}

make() {
	pathpat="^.*:[0-9]+"
	#pathpat="(/[^/]*)+:[0-9]+"
	ccred=$(echo -e "\033[0;31m")
	cyellow=$(echo -e "\033[0;33m")
	ccend=$(echo -e "\033[0m")
	/usr/bin/make "$@" 2>&1 | sed -E -e "/[Ee]rror[: ]/ s%$pathpat%$ccred&$ccend%g" -e "/[Ww]arning[: ]/ s%$pathpat%$ccyellow&$ccend%g"
	return ${PIPESTATUS[0]}
}


updateBash() {
	currentFolder=$(pwd)

	cd /Users/Pixowl/work/dot_files
	git pull
	cd $currentFolder
}

reloadBash() {
	. ~/.bash_profile
}

hg_branch_update() {
	hg prompt "{[{branch}]}{update}" 2> /dev/null
}

hg_in_out() {
	hg prompt "{[in:{incoming|count}]}{[out:{outgoing|count}]}" 2> /dev/null
}

hg_st() {
	hg prompt "{status}{status}{status}" 2> /dev/null
}

ignorehg() {
	if [[ -z "$1" ]]; then
		echo Usage: 'ignorehg path pattern' or 'ignorehg pattern'
		return
	elif [[ -z "$2" ]]; then
			hgignoreFile=.hgignore
			pattern=$1
	else
		hgignoreFile=$1/.hgignore
		pattern=$2
	fi

	if [ ! -f hgignoreFile ]; then
		while true; do
			read -p "file .hgignore not found. create it now? (y/n)" yn
				case $yn in
					[Yy]* ) echo $pattern >> $hgignoreFile; return;;
					[Nn]* ) return;;
					* ) echo "Please answer yes or no.";;
			esac
		done
	fi
}

hg_remove_new_unversioned() {
	filesToRemove=`hg st | grep "^? " | sed 's/^\? //g'`
	echo "Unversioned files:"
	echo "$filesToRemove" | sed 's/^/	/g'

	read -r -p "Remove? (Y/n)" response
	response=`tr [A-Z] [a-z] <<< "$response"`
	if [[ "yes y " =~ (^|[[:space:]])"$response"($|[[:space:]]) ]] ; then
		echo "$filesToRemove" | sed 's/ /\\ /g' | xargs rm -r 
		echo "Done."
	else
		echo "Aborted."
	fi
}

# Simple svn enhancement function
# Read more about it at: http://westhoffswelt.de
function svnColor() {
	local SVN="`which svn`"

	local operation=$1
	shift
	
	case "${operation}" in
		"status"|"stat"|"st")
			# My thanks for this snippet go to Kore Nordmann
			# (http://kore-nordmann.de)
    		${SVN} ${operation} --ignore-externals "$@" | grep -v '^X' | sed -e 's/^\?.*$/[1;34m\0[m/' -e 's/^!.*$/[1;31m\0[m/' -e 's/^A.*$/[1;32m\0[m/' -e 's/^M.*$/[1;33m\0[m/' -e 's/^D.*$/[0;31m\0[m/'
		;;
		"diff"|"di")
			# The colordiff utility (http://colordiff.sourceforge.net) is
			# needed for this enhancement to work
			# Thanks to Lukas Kahwe Smith for the addition to
			# leave out whitespace changes
			# Thanks to Robin Speekenbrink for the hint to the -R
			# switch of less, which should fix problems some of
			# you might have with colorization.
			${SVN} ${operation} "$@" --diff-cmd `which diff` -x "-u -w"|colordiff|less -R
		;;
		"update"|"up")
			# This snippet is taken from a blog post found on the net. My
			# thanks go out to the author of it.
			# http://woss.name/2007/02/01/display-svn-changelog-on-svn-up/
			# I have slightly modified it to better suit my needs.
			
			local old_revision=`${SVN} info "$@" | awk '/^Revision:/ {print $2}'`
			local first_update=$((${old_revision} + 1))
			
			${SVN} ${operation} "$@"
		    
			local new_revision=`${SVN} info "$@" | awk '/^Revision:/ {print $2}'`

			if [ ${new_revision} -gt ${old_revision} ]; then
				svn log -v -rHEAD:${first_update} "$@"
			else
				echo "No changes."
			fi
		;;
		"log")
			${SVN} ${operation} "$@"|sed -e 's/^-\+$/[1;32m\0[m/' -e 's/^r[0-9]\+.\+$/[1;31m\0[m/'
		;;
		"blame"|"praise"|"annotate"|"ann")
			${SVN} ${operation} "$@"|sed -e 's/^\(\s*[0-9]\+\s*\)\([^ ]\+\s*\)\(.*\)$/[1;32m\1[m[1;31m\2[m\3/'
		;;
		*)
			${SVN} ${operation} "$@"
		;;
	esac
}

export PS1='\A|\u|\w  $ '
#export PS1='\A|\u|\w $(hg_branch_update)\[\033[0;31m\]$(hg_st)\[\033[0m\]$ '

LSCOLORS=exfxcxdxbxegedabagacad
