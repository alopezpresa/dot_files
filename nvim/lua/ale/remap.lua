
vim.g.mapleader = " "
vim.keymap.set("n", "<leader>fe", vim.cmd.Ex)

-- 
vim.keymap.set("n", "<Tab>=", "mtvi{=`t:delmarks t<CR>")

vim.keymap.set("n", "<Esc><Esc>", ":noh<Esc><Esc>")

if vim.loop.os_uname().sysname == "Darwin" then
    vim.keymap.set("n", "∆", ":m .+1<CR>==")
    vim.keymap.set("n", "˚", ":m .-2<CR>==")
    vim.keymap.set("i", "∆", "<Esc>:m .+1<CR>==gi")
    vim.keymap.set("i", "˚", "<Esc>:m .-2<CR>==gi")
    vim.keymap.set("v", "∆", ":m '>+1<CR>gv=gv")
    vim.keymap.set("v", "˚", ":m '<-2<CR>gv=gv")
else
    vim.keymap.set("n", "<A-j>", ":m .+1<CR>==")
    vim.keymap.set("n", "<A-k>", ":m .-2<CR>==")
    vim.keymap.set("i", "<A-j>", "<Esc>:m .+1<CR>==gi")
    vim.keymap.set("i", "<A-k", "<Esc>:m .-2<CR>==gi")
    vim.keymap.set("v", "<A-j>", ":m '>+1<CR>gv=gv")
    vim.keymap.set("v", "<A-k>", ":m '<-2<CR>gv=gv")
end

-- greatest remap ever
vim.keymap.set("x", "<leader>kp", [["_dP]])

-- next greatest remap ever : asbjornHaland
vim.keymap.set({"n", "v"}, "<leader>y", [["+y]])

-- next greatest remap ever : asbjornHaland
vim.keymap.set({"n", "v"}, "<leader>cy", [["+y]])
vim.keymap.set("n", "<leader>cY", [["+Y]])

-- this one doesn't work?
vim.keymap.set({"n", "v"}, "<leader>cd", [["+d]])

-- and muh version for paste
vim.keymap.set({"n", "v"}, "<leader>cp", [["+p]])
vim.keymap.set("n", "<leader>cP", [["+P]])
vim.keymap.set("n", "<leader>CP", [["+P]])
--
-- This is going to get me cancelled
vim.keymap.set("i", "<C-c>", "<Esc>")

vim.keymap.set("n", "Q", "<nop>")
vim.keymap.set("n", "<C-f>", "<cmd>silent !tmux neww tmux-sessionizer<CR>")
vim.keymap.set("n", "<leader>f", vim.lsp.buf.format)

vim.keymap.set("n", "<C-k>", "<cmd>cnext<CR>zz")
vim.keymap.set("n", "<C-j>", "<cmd>cprev<CR>zz")
vim.keymap.set("n", "<leader>k", "<cmd>lnext<CR>zz")
vim.keymap.set("n", "<leader>j", "<cmd>lprev<CR>zz")

vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })

vim.keymap.set("n", "<leader>vpp", "<cmd>e ~/.dotfiles/nvim/.config/nvim/lua/theprimeagen/packer.lua<CR>");
vim.keymap.set("n", "<leader>mr", "<cmd>CellularAutomaton make_it_rain<CR>");

vim.keymap.set("n", "<leader><leader>", function()
    vim.cmd("so")
end)


vim.keymap.set({"n","v","i"}, "<C-s>", function() vim.cmd("w") end)

