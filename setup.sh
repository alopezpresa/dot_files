#!/bin/bash

DOT_FILES_FOLDER=$(pwd)

programs=( "vim" "make" "gcc" "g++" "gdb" "valgrind" "colordiff" "mercurial" "subversion" "cloc" "cppcheck" )

ln -s $DOT_FILES_FOLDER/gitconfig ~/.gitconfig
ln -s $DOT_FILES_FOLDER/gitignore_global ~/.gitignore_global
ln -s $DOT_FILES_FOLDER/hgrc ~/.hgrc
ln -s $DOT_FILES_FOLDER/hg/ ~/.hg-ext
ln -s $DOT_FILES_FOLDER/vimrc ~/.vimrc

rm -rf ~/.vim
ln -s $DOT_FILES_FOLDER/vim/ ~/.vim

ln -s $DOT_FILES_FOLDER/bash ~/.bash_custom
cp $DOT_FILES_FOLDER/bash_path ~/.bash_path

BASHRC_KEY='#SETUP-bashrc-include'
touch ~/.bashrc
if ! grep -q $BASHRC_KEY ~/.bashrc ; then
	echo "" >> ~/.bashrc
	echo $BASHRC_KEY >> ~/.bashrc
	echo "#added by $DOT_FILES_FOLDER/setup.sh" >> ~/.bashrc
	echo "if [ -f $DOT_FILES_FOLDER/bashrc ]; then" >> ~/.bashrc
	echo "	source ~/.bash_custom" >> ~/.bashrc
	echo "fi" >> ~/.bashrc
	echo "" >> ~/.bashrc
fi

touch ~/.bash_profile
if ! grep -q $BASHRC_KEY ~/.bash_profile ; then
	echo "" >> ~/.bash_profile
	echo $BASHRC_KEY >> ~/.bash_profile
	echo "#added by $DOT_FILES_FOLDER/setup.sh" >> ~/.bash_profile
	echo "if [ -f ~/.bashrc ]; then" >> ~/.bash_profile
	echo "	source ~/.bashrc" >> ~/.bash_profile
	echo "fi" >> ~/.bash_profile
	echo "" >> ~/.bash_profile
	echo "currentFolder=\$(pwd)" >> ~/.bash_profile
	echo "" >> ~/.bash_profile
	echo "cd $DOT_FILES_FOLDER" >> ~/.bash_profile
	echo "git pull" >> ~/.bash_profile
	echo "cd \$currentFolder" >> ~/.bash_profile
	echo "" >> ~/.bash_profile
else
	echo ".bash_profile already set"
fi

if [ "$(uname)" == "Darwin" ]; then
    # xcode stuffm        
	rm -r ~/Library/Developer/Xcode/UserData/FontAndColorThemes
	ln -s $DOT_FILES_FOLDER/mac/xcode-colors ~/Library/Developer/Xcode/UserData/FontAndColorThemes

	rm -r ~/Library/Developer/Xcode/UserData/KeyBindings
	ln -s $DOT_FILES_FOLDER/mac/xcode-shortcuts ~/Library/Developer/Xcode/UserData/KeyBindings

#########
## Disabled stuff because lots of things could've changed since the last time I used a Mac

#    # improve simulator performance (system will consume more energy)        
#	sudo touch /etc/sysctl.conf
#	TIMER_COALESCING_SETTING='kern.timer.coalescing_enabled=0'
#	if ! grep -q $TIMER_COALESCING_SETTING /etc/sysctl.conf ; then
#		sudo echo $TIMER_COALESCING_SETTING >> /etc/sysctl.conf
#	else
#		echo "timer coalescing already set"
#	fi
#
#	# system shortcuts 
#	rm -r ~/Library/KeyBindings
#	ln -s $DOT_FILES_FOLDER/mac/shortcuts ~/Library/KeyBindings
#	defaults write -g ApplePressAndHoldEnabled -bool false
#

	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

	for program in "${programs[@]}" ; do
		echo ""
		echo "Installing '${program}. . .' "
		brew install "${program}"
	done

elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
	for program in "${programs[@]}" ; do
		echo ""
		echo "Installing '${program}. . .' "
		sudo apt-get install "${program}"
	done

	INSTALL_OPENBOX_PREFS=`openbox --version | grep Openbox`

	if [[ $INSTALL_OPENBOX_PREFS == *Openbox* ]] ; then
		rm -r ~/.config/openbox
		ln -s $DOT_FILES_FOLDER/openbox/ ~/.config/openbox
	fi
#elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
#    # Do something under Windows NT platform
fi
