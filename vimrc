
execute pathogen#infect()

""" WINDOWS PATCHS . . .
if has('win32') || has('win64')
"  set runtimepath=$HOME/.vim,$VIM/vimfiles,$VIMRUNTIME,$VIM/vimfiles/after,$HOME/.vim/after
endif

""" FILE TYPE OPTIONS
set ff=unix
filetype on
au BufNewFile,BufRead *.lmx set filetype=xml
au BufNewFile,BufRead *.plist set filetype=xml
au BufNewFile,BufRead *.nut set filetype=squirrel
au BufNewFile,BufRead *.as set filetype=actionscript
au BufNewFile,BufRead *.di set filetype=javascript
au BufNewFile,BufRead *bash* set filetype=sh

""" VISUAL OPTIONS
set mouse=a
syntax on
set cursorline
set nonumber
set wrap linebreak nolist
"set bg=dark
"colors hybrid
" Highlight searchs by default, and make double-<Esc> clear search highlights
set hlsearch
nnoremap <silent> <Esc><Esc> <Esc>:nohlsearch<CR><Esc>
" Tabs options
nnoremap <C-Tab> <C-PageUp>
nnoremap <C-Tab> <C-PageUp>
inoremap <C-S-Tab> <C-PageDown>
inoremap <C-S-Tab> <C-PageDown>
nnoremap <C-S-t> :tabnew<CR>

""" TEXT FORMAT OPTIONS
set backspace=2
set smartindent
set ls=2
set shiftwidth=4
set tabstop=4
set expandtab

""" TEXT HANDLING OPTIONS
""" Moving Lines: http://vim.wikia.com/wiki/Moving_lines_up_or_down
""" I tweaked it in an attempt to make this work well with VSVim
nnoremap <A-k> :m -2<CR>==
nnoremap <A-j> :m +1<CR>==

inoremap <A-j> <Esc>:m +1<CR>==gi
inoremap <A-k> <Esc>:m -2<CR>==gi

vnoremap <A-k> :m '<-2<CR>gv=gv
vnoremap <A-j> :m '>+1<CR>gv=gv

nnoremap <A-k> :m .-2<CR>==
nnoremap <A-j> :m .+1<CR>==
inoremap <A-k> <Esc>:m .-2<CR>==gi
inoremap <A-j> <Esc>:m .+1<CR>==gi
vnoremap <A-k> :m '<-2<CR>gv=gv
vnoremap <A-j> :m '>+1<CR>gv=gv


""" FUNCTIONS AND COMMANDS
command! SpaceIndent set tabstop=4 | set expandtab
command! TabIndent set tabstop=4 | set noexpandtab

" Move to next/prev compile error
map ]e :cnext<CR>
map [e :cprev<CR>

" Indent { } scope
map <TAB>= vi}=<C-o>

""" stolen from http://vim.wikia.com/wiki/Display_output_of_shell_commands_in_new_window
function! s:ExecuteInShell(command)
  let command = join(map(split(a:command), 'expand(v:val)'))
  let winnr = bufwinnr('^' . command . '$')
  silent! execute  winnr < 0 ? 'botright new ' . fnameescape(command) : winnr . 'wincmd w'
  setlocal buftype=nowrite bufhidden=wipe nobuflisted noswapfile nowrap number
  echo 'Execute ' . command . '...'
  silent! execute 'silent %!'. command
  silent! execute 'resize ' . line('$')
  silent! redraw
  silent! execute 'au BufUnload <buffer> execute bufwinnr(' . bufnr('#') . ') . ''wincmd w'''
  silent! execute 'nnoremap <silent> <buffer> <LocalLeader>r :call <SID>ExecuteInShell(''' . command . ''')<CR>'
  echo 'Shell command ' . command . ' executed.'
endfunction
command! -complete=shellcmd -nargs=+ Shell call s:ExecuteInShell(<q-args>)
command! BS buffers

""" STATUS LINE
set statusline=%t       "tail of the filename
set statusline+=%m      "modified flag
set statusline+=%r      "read only flag
set statusline+=:%l:%c  " :line:column
set statusline+=%=      "left/right separator
set statusline+=%l/%L   "cursor line/total lines and cursor column
set statusline+=\ %p%% "percent through file
set statusline+=\ [%{strlen(&fenc)?&fenc:'none'}, "file encoding
set statusline+=%{&ff}] "file format
set statusline+=%h      "help file flag
set statusline+=%y      "filetype

""" GUI OPTIONS
set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar

""" Load .vimrc in the current dir
"if getcwd()~=#'^\(/my/safe/dir1/\|/my/safe/dir2/\)'
  "set secure exrc
  set exrc
"endif

set belloff=all


""" Mac stuff aaaaaa
set guifont=Menlo\ Regular:h16
